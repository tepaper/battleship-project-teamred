export const grid = [
  // 0 tyhjä, ei ammuttu  ~
  // 1 tyhjä, ammuttu     .
  // 2 laiva, ei ammuttu  ~
  // 3 laiva, ammuttu     X

  [0, 2, 2, 0, 0,], 
  [0, 2, 0, 0, 0,],
  [0, 0, 0, 0, 0,],
  [0, 0, 0, 0, 2,],
  [0, 0, 0, 0, 2,]
];

export function modifyGrid (y, x, status) {
grid[y][x] = status
}

export function printGrid(grid) {
  for (let i = 0; i < grid.length; i++) {
      let row = "";
      for (let cell of grid[i]) {
              if (cell === 3) {      
                  row += "X ";                
              } else if (cell === 2) {
                  row += "~ ";
              } else if (cell === 1) {
                  row += ". ";
              } else if (cell === 0) {
                  row += "~ ";
              }
          }
          console.log(row);
      }
  }