import readLine from "readline/promises";
import { printGrid, grid, modifyGrid } from "./grid.js";

const reader = readLine.createInterface(process.stdin, process.stdout);

printGrid(grid);

function letterToNumericCoordinate(letter) {
  if (letter === "A") return 0;
  if (letter === "B") return 1;
  if (letter === "C") return 2;
  if (letter === "D") return 3;
  if (letter === "E") return 4;
}

//gameplay loop
while (true) {
  const input = await reader.question("Enter coordinates to attack: \n");
  console.log("You hit:", input);

  if (input === "quit") {
    console.log("Game over!");
    reader.close();
    break;
  } else if (input.length !== 2) {
    console.log("Invalid coordinates.");
  }

  const coordinates = inputToCoordinates(input);
  const osuiko = isItHit(coordinates[0], coordinates[1], input, grid);
  // console.log(coordinates); for testing
  // console.log(osuiko); for testing
  printGrid(grid);
  const enemyShipsLeft = countEnemyShips(grid);

  if (enemyShipsLeft === 0) {
    console.log("Victory!");
    reader.close();
    break;
  }
  console.log("How many enemy ships are left:", enemyShipsLeft);
}

function inputToCoordinates(input) {

  const chars = input.split('');
  const letter = chars[0].toUpperCase();
  const number = chars[1];

  let yCoordinate = letterToNumericCoordinate(letter);
  let xCoordinate = number - 1;

  return [yCoordinate, xCoordinate];
}

function isItHit(y, x, userInput, grid) {
  //console.log("y:",y); for testing
  //console.log("x:",x); for testing
  if (grid[y][x] === 2) {
    modifyGrid(y, x, 3);
    console.log("You hit an enemy boat!");
    return true;
  }
  else if (grid[y][x] === 0) {
    modifyGrid(y, x, 1);
    console.log("You missed!");
    return true;
  }

  else if (grid[y][x] === 1 || (grid[y][x] === 3)) {
    console.log("You have already shot: ", userInput.toUpperCase());
    return true;
  }
}

function countEnemyShips(grid) {
  let count = 0;
  for (let row of grid) {
    for (let cell of row) {
      if (cell === 2) {
        count++;
      }
    }
  }
  // console.log("count is:", count); for testing
  return count;
}