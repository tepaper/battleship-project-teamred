import readLine from "readline/promises"
const reader = readLine.createInterface(process.stdin, process.stdout)

// initialize variables
let myGridSize = 5; //if we want, we can ask player how long grid they want
let enemyGridSize = 5;
let myGrid = createGrid(myGridSize);
let enemyGrid = createGrid(enemyGridSize);
let myShips = 5;            //when enemy or myships ends to 0, game ends
let enemyShips = 5;         //decrement these when ships are hit, 5 --> 4 --> 3 --> 2 --> 1 --> 0
let enemyLocations = {};    //empty object

printGrid(enemyGrid, true);      //true is enemy parameter
printGrid(myGrid);               //no need to add another parameter because isEnemy is false by default

//question loop ship placement, game setup loop
//Taski: vie indeksiin ja yhdistä aiemman koodin kanssa. Muuta ampumismuotoon, eli looppi kestää niin pitkään, kunnes jomman kumman laivat on decrementoitunut 0:aan ja tulee game over.
for (let i = 1; i < 6; i++) {                //i starts with 1, instead of normally 0, because we don't want to ask player about ships 0, 1 and 2, instead we want to ask 1, 2 and 3
    let x = await reader.question("Enter the x coordinate for your ship number " + i + "\n");
    //second i is 6, because we have 5 ships
    let y = await reader.question("Enter the y coordinate for your ship number " + i + "\n");

    placeCharacter(x, y, "X", myGrid);       //placeCharacter let us know where our ships are
    placeRandomCharacter("X", enemyGrid, enemyGridSize);   //placeRandomCharacter randomize enemyships within board
    drawBreak();                             //visual stuff
    printGrid(enemyGrid, true);              //print again grids
    printGrid(myGrid);
}

//gameplay loop
while (enemyShips > 0 && myShips > 0) {
    let x = await reader.question("Enter the x coordinate for your attack \n");
    let y = await reader.question("Enter the y coordinate for your attack \n");

    if (attack(x, y, enemyGrid)) {   //player attacks
        enemyShips--;               //decrements enemyships
    }

    x = getRandomInt(myGridSize);   //enemy attacks
    y = getRandomInt(myGridSize);
    if (enemyShips > 0 && attack(x, y, myGrid)) {   //enemyShips checks if enemy is still has ships and can play
        myShips--;                  //decrements myships
    }

    drawBreak();                    //visual stuff
    printGrid(enemyGrid, true);              //print again grids to show attacks
    printGrid(myGrid);
}

//if else statements of winning and losing
if (myShips < enemyShips) {
    console.log("You lose")
} else {
    console.log("You win!")
}

function createGrid(size) {
    let grid = [];
    for (let i = 0; i < size; i++) { // i and j creates x and y grid
        grid[i] = [];
        for (let j = 0; j < size; j++) {
            grid[i][j] = "~";
        }
    }
    return grid;
}
function printGrid(grid, isEnemy = false) {       //with isEnemy, we can hide enemy board
    const headers = createHeaders(grid.length);    //outer headers shows player in which column we are, 1,2,3, so on
    console.log(headers);
    for (let i = 0; i < grid.length; i++) {
        let rowStr = i + " ";
        for (let cell of grid[i]) {
            if (isEnemy && cell == "X") {      //X is shown to player when shot on right place
                rowStr += "~ ";                 //changing this we can see which randomized spots enemy chose
            } else {
                rowStr += cell + " ";
            }
        }
        console.log(rowStr);
    }
}
//taski, ei niin tärkeä: Vasen pystyreuna muuttaa A,B,C,D,E -muotoon ja ylä horisontti 1,2,3,4,5
function createHeaders(size) {      //issues with headers, need to change left vertical side to letters
    let result = "  ";      //with these spaces there will be more space between characters
    for (let i = 0; i < size; i++) {
        result += i + " ";
    }
    return result;
}
function placeCharacter(x, y, z, grid) {
    grid[y][x] = z;
}
function placeRandomCharacter(z, grid, max) {    //max makes sure that randomized enemyships are within board
    let didPlace = false;                       //this checks if randomized spot has already been taken
    while (!didPlace) {
        let x = getRandomInt(max);
        let y = getRandomInt(max);
        if (!enemyLocations[`${x}-${y}`]) {            //if randomized spot has already been taken, continue loop to find a empty spot
            placeCharacter(x, y, z, grid);
            didPlace = true;
            enemyLocations[`${x}-${y}`] = true;
        }
    }
}

function getRandomInt(max) {            //gets random integer between zero to max value
    return Math.floor(Math.random() * Math.floor(max));
}

function attack(x, y, grid) {
    if (grid[y][x] == "X") {        //if location has ship, and ship mark is X
        grid[y][x] = "!";           //mark as a hit
        return true;
    } else if (grid[y][x] == "~") {
        grid[y][x] = ".";               //was a miss
        return false;
    } else {
        return false;
    }
}

function drawBreak() {      //visual addition, we should check should we use another method which clear visual view
    console.log("-----------------------------------------------------------------");
}